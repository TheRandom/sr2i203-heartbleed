# Vanquish is a tool to recon and exploit heartbleed and shellshock vulnerability.
# Copyright (C) 2022  Théophile Reverdell, Isaïe Muron, Erwan Glaziou, Clara Delabrouille
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# test/test_vanquish.py

# Common modules
import pytest
from vanquish import __app_name__, cli

#### CLI TEST ####
# Cli test modules

from click.testing import CliRunner


# test cli function
def test_cli_heartbleed():
    # Create object to run code as cli
    runner = CliRunner()
    # Call function from cli.py and add args
    result = runner.invoke(cli.func_call, ['--tool=heartbleed', '--ip=127.0.0.1'])
    assert result.exit_code == 0
    assert result.output == 'Calling heartbleed scripts...\n'


def test_cli_shelshock():
    runner = CliRunner()
    result = runner.invoke(cli.func_call, ['--tool=shellshock', '--ip=127.0.0.1'])
    assert result.exit_code == 0
    assert result.output == 'Calling shellshock scripts...\n'


def test_cli_ip_none():
    runner = CliRunner()
    result = runner.invoke(cli.func_call, ['--tool=shellshock', '--ip=none'])
    assert result.exit_code == 0
    assert result.output == 'Please provide an IP adress (range) to use the tool on.\n'


# Errors tests
# FIXME: test is not good
def test_cli_ip_fake():
    runner = CliRunner()
    result = runner.invoke(cli.func_call, ['--tool=shellshock', '--ip=wrong_ip_format'])
    assert result.exit_code == 0
    # assert result.output == 'Unreachable code\n'


# FIXME: test is not good
def test_cli_no_args():
    runner = CliRunner()
    result = runner.invoke(cli.func_call, [])
    # assert result.exit_code == 2
    # assert result.output == "Error: Missing option '--tool' / '-t'\n"


#### SHELLSHOCK TEST ####


#### HEARTBLEED TEST ####

# Test main function of heartbleed module
@pytest.mark.xfail(raises=SystemExit)
def test_exploit_heartbleed():
    cli.heartbleed.exploit("127.0.0.1")


# test h2bin
def test_h2bin():
    assert b'\xff' == cli.heartbleed.h2bin('''ff''')


# TODO: Add unit test for each functions

#### RECOGNITION TEST ####


if __name__ == '__main__':
    pytest.main(["test_vanquish.py"])
