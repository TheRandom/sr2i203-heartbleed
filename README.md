# SR2I203-HEARTBLEED-SHELLSHOCK
Project related to the SR2I204 course. It encompasses development of Scapy scripts to demonstrate the Heartbleed and Shellshock exploits.



## Getting started
Clone the repository 
`$ git clone https://gitlab.com/TheRandom/sr2i203-heartbleed`


## Test and Deploy
This section will contains every test and deployment tasks that we run on our project.


## Name
```
 ____   ____                          .__       .__     
 \   \ /   /____    ____   ________ __|__| _____|  |__  
  \   Y   /\__  \  /    \ / ____/  |  \  |/  ___/  |  \ 
   \     /  / __ \|   |  < <_|  |  |  /  |\___ \|   Y  \
    \___/  (____  /___|  /\__   |____/|__/____  >___|  /
                \/     \/    |__|             \/     \/ 
```

## Description
Vulnerability ANalysis QUery Implementation for Shellshock and Heartbleed : VANQUISH.

Vanquish is a tool to recon and exploit heartbleed and shellshock vulnerability.


## Installation
To install required modules run the following command
`$ pip install -r requirements.txt`

Or run
`$ make setup`

## Usage
This tool will be a CLI tool. *Documentation will go here*


`$ python3 vanquish --tool=<tool> --ip=<ip>`

For help use :

`$ python3 vanquish --help`

## Testing
To run test use the following command
`$ pytest`

Or run :
`$ make test`


## Architecture
Architecture of interaction of our python modules.

```
                     +--------------+
                     |              |
                     |    Command   |
Exploit directly     |    Line      |        Exploit directly a target
a target +-----------+    Interface +-----------+ 
         |           |              |           |
         |           |              |           |
         |           +----+---------+           |
         |                |                     |
         |                |                     |
         |                |                     |
         |                |                     |
         |                |                     |
         |                |                     |
         |                |                     |
+--------+-----+     +----+---------+    +------+-------+
| exploit      |     |              |    | exploit      |
| shellshock   |     |  nmap scan   |    | heartbleed   |
|              |     |              |    |              |
|              |     |              |    |              |
|              |     |              |    |              |
|              |     |              |    |              |
|              |     |              |    |              |
+--------------+     +--------------+    +--------------+
```

## License

Vanquish is a tool to recon and exploit heartbleed and shellshock vulnerability.\
Copyright (C) 2022  Théophile Reverdell, Isaïe Muron, Erwan Glaziou, Clara Delabrouille

**Concerning the associated code**:\
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Authors and acknowledgment
Authors : Clara, Erwan, Isaïe and Théophile.

## Legal Disclaimer
This project is for educational purpose only. We do not hold responsibility over the use made of this code.

## Project status
WIP

## TODO: 
- [x] add POC to exploit heartbleed vulnerability
- [x] Add exploitation of heartbleed vulnerability
- [x] Add unit test for heartbleed vulnerability exploitation
- [x] Add function call to modules for cli.py
- [x] Add unit test for cli
- [ ] Add recongnition ip scan module
- [ ] Add unit test for recognition module
- [ ] add POC to exploit shellshock vulnerability
- [ ] Add exploitation of shellshock vulnerability
- [ ] Add unit test for shellshock vulnerability exploitation

# Bibliography
- Wikipedia webpage of the Heartbleed exploit: https://en.wikipedia.org/wiki/Heartbleed
- Heartbleed dedicated website: https://heartbleed.com/
- Heartbleed dedicated page of the exploit-db website: https://www.exploit-db.com/exploits/32745
- Wikipedia webpage of the Shellshock exploit: https://en.wikipedia.org/wiki/Shellshock_(software_bug)
- Shellshock exploit report: https://www.exploit-db.com/docs/48112
- Wikipedia webpage of the TLS protocol: https://en.wikipedia.org/wiki/Transport_Layer_Security
- Vulnerability test explainations: https://koesio.com/infrastructure-it-reussir-un-test-de-vulnerabilite/
- ?: https://hub.docker.com/r/hmlio/vaas-cve-2014-6271/
