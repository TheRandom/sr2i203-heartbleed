---
title: 'SR2I-203 Project Report'
author: Théophile Reverdell, Isaïe Muron, Erwan Glasziou, Clara Delabrouille
...


\newpage
## Preamble

Vulnerability ANalysis QUery Implementation for Shellshock and Heartbleed : VANQUISH.
```
 ____   ____                          .__       .__     
 \   \ /   /____    ____   ________ __|__| _____|  |__  
  \   Y   /\__  \  /    \ / ____/  |  \  |/  ___/  |  \ 
   \     /  / __ \|   |  | |_|  |  |  /  |\___ \|   Y  \
    \___/  (____  /___|  /\__   |____/|__/____  >___|  /
                \/     \/    |__|             \/     \/ 
```
\

### License

Vanquish is a tool to recon and exploit heartbleed and shellshock vulnerability.\
Copyright (C) 2022  Théophile Reverdell, Isaïe Muron, Erwan Glaziou, Clara Delabrouille

**Concerning the associated code**:\
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

\

### Getting started

This project consists in the development of a CLI tool to identify vulnerable environments to shellshock and heartbleed vulnerability and to exploit them.

Clone the [project repository](https://gitlab.com/TheRandom/sr2i203-heartbleed.git) using the following command :\
`$ git clone https://gitlab.com/TheRandom/sr2i203-heatbleed.git`


**Information about requirements, setting-up and running the code is provided in the README.md file**. This report only encompasses the usage, observations and technicalities made throughout the project. The references used can be found in both this report and the README.md file.


\newpage
## Project Architecture

### Architecture Diagram

```
                     +--------------+
                     |              |
                     |    Command   |
Exploit directly     |    Line      |        Exploit directly a target
a target +-----------+    Interface +-----------+ 
         |           |              |           |
         |           |              |           |
         |           +----+---------+           |
         |                |                     |
         |                |                     |
         |                |                     |
         |                |                     |
         |                |                     |
         |                |                     |
         |                |                     |
+--------+-----+     +----+---------+    +------+-------+
| exploit      |     |              |    | exploit      |
| shellshock   |     |  nmap scan   |    | heartbleed   |
|              |     |              |    |              |
|              |     |              |    |              |
|              |     |              |    |              |
|              |     |              |    |              |
|              |     |              |    |              |
+--------------+     +--------------+    +--------------+
```

Each block represented corresponds to a python module.


### Implementation

The tool is composed of a single interface module calling external code to scan for/use the specified exploit.


**Firstly we have our command line interface**. It is a program that will interact with arguments provided on it's execution :

- `--tool=<tool>` : indicate which exploit we use, it could be nmap, heartbleed or shellshock.
- `--ip=<ip>` : indicate which ip (or ip range) is targeted.



**After parsing arguments, the cli module will call the tool asked with the ip provided as an argument**.

**The heartbleed module will initiate the exploit function**.
The exploit function will initiate a connection on the port listening for TLS connection.
After that it will send the heartbleed exploit code to see what the server will respond.
If more data is sent to the client than it should then it can exploit heartbleed vulnerability to read memory of the server.


\newpage
## Usage

**Run the following command to launch the project** :\
`$ python3 vanquish --tool=<tool> --ip=<ip>`

More accurate description of each arguments will be available using the following command :\
`$ python3 vanquish --help`

## Exploit description

### Heartbleed

\
Heartbeat is a standard (RFC 6520) to keep a secure connection alive without renegotiating the secure tunnel.
This standard was implemented by `openssl` in 2011. This implementation contained a bug which is called heartbleed vulnerability.
The heartbleed vulnerability is called a Buffer over-read. It is a bug where a process is reading more data than it should from a buffer.

A normal Heartbeat request correspond to a payload sent by the client to a server (normally a text string with the payload size). The server has to send back the same payload to the client.

The vulnerability exist because no verification of the payload size announced by the client is made. In fact the `openssl` program allocate exactly the size announced by the client without checking the size of the string sent. This is why the `openssl` program is reading too much memory and sending it back to the client.


A **normal request** would look like this:

```
+-------------+                                  +-------------+
|             |   ("text", lenghth=4)            |             |
|             +---------------------------------->             |
|  Client     |                                  |   Server    |
|             |                                  |             |
|             <----------------------------------+             |
|             |                  ("text")        |             |
|             |                                  |             |
+-------------+                                  +-------------+
```


However, a **malicious request** looks like this. Note that this would give the client 496 characters that he should not have access to.
```
+-------------+                                  +-------------+
|             |   ("text", lenghth=500)          |             |
|             +---------------------------------->             |
|  Attacker   |                                  |   Server    |
|             |                                  |             |
|             <----------------------------------+             |
|             |  ("text + Leaked Memory")        |             |
|             |                                  |             |
+-------------+                                  +-------------+
```

The attack consist of sending a small payload with a large length to over read memory.
However, the attacker is not able to choose the location of the memory he can read.
If he gets lucky he can get access to sensitive data and can compromise secure connections.

#### How our heartbleed module works

The client will initiate a secure connection with the server (Using TLS). After this connection is initiated, the client will send a heartbeat forged request (with malicious size of payload).\
The server will then respond with more data than it should send. The client can know see memory leak in the response.


### Shellshock

\
Shellshock (or Bashdoor) is a vulnerability discovered in 2014 for the GNU Bash shell. 

Basically, giving it the sequence of characters `(){:;};` makes vulnerable versions of Bash execute the intructions following this string without any verification. This is due to the fact that the sequence corresponds to the definition of a function.

As such, one can check if a system's Bash shell is vulnerable by giving it the instruction : 

`$ env x='() { :;}; echo vulnerable' bash -c "echo test"`

In this example, we define an environment variable `x` and give it the value `(){ :;}`, thus defining it as a environment function. However the interesting part is the `echo vulnerable` following it, which will cause the shell to print `vulnerable` should it be vulnerable to Shellshock : the echo call is executed without verification.\
Finally the `bash -c "echo test"` is just there to check if the variable declaration is read at all. If it isn't it means you probably don't even have access to the bash shell at all. 

To capitalize on this vulnerability, you only have to change the injected query to the Bash shell from `echo vulnerable` to something more destructive such as `rm -rf /*` or `cat /etc/passwd`. 

We are getting to the Bash shell through a CGI (Common Gateway Interface) script that uses Bash. This is important because the CGI script is given information by the web server (such as headers) by defining environment variables, and this how we get to exploit the vulnerability. 

In the proof of concept we use (courtesy of Emre Bastuz) the command `curl -H` to manipulate the header in order to define the environment function followed by the malicious instructions.


\newpage
## Future additions

### Feature-wise 

- Add exploitation of shellshock vulnerability within a Scapy script by crafting the proper packet structure.
- Add unit test for recognition module and shellshock
- add POC to exploit shellshock vulnerability


### Others

- Make a more complete documentation for the usage of the source code


# Bibliography
- Wikipedia webpage of the Heartbleed exploit: https://en.wikipedia.org/wiki/Heartbleed
- Heartbleed dedicated website: https://heartbleed.com/
- Heartbleed dedicated page of the exploit-db website: https://www.exploit-db.com/exploits/32745
- Wikipedia webpage of the Shellshock exploit: https://en.wikipedia.org/wiki/Shellshock_(software_bug)
- Shellshock exploit report: https://www.exploit-db.com/docs/48112
- Wikipedia webpage of the TLS protocol: https://en.wikipedia.org/wiki/Transport_Layer_Security
- Vulnerability test explainations: https://koesio.com/infrastructure-it-reussir-un-test-de-vulnerabilite/
- Docker used as a target to showcase shellshock vulnerability: https://hub.docker.com/r/hmlio/vaas-cve-2014-6271/
