# Vanquish is a tool to recon and exploit heartbleed and shellshock vulnerability.
# Copyright (C) 2022  Théophile Reverdell, Isaïe Muron, Erwan Glaziou, Clara Delabrouille
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# vanquish/__init__.py

__app_name__ = "vanquish"
__version__ = "0.1.0"
