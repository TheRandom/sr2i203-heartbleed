# Vanquish is a tool to recon and exploit heartbleed and shellshock vulnerability.
# Copyright (C) 2022  Théophile Reverdell, Isaïe Muron, Erwan Glaziou, Clara Delabrouille
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# vanquish/cli.py
from vanquish import __app_name__, heartbleed, shellshock, recognition

import click


# Arguments
@click.command()
@click.option("--tool", '-t', 'tool', required=True,
              help="--tool=<name> Possible values : nmap, heartbleed, shellshock, help")
@click.option("--ip", 'ip', default="none",
              help="--ip=<ip> Format accepted : A.B.C.D with A, B, C, D integers between 0 and 255. They shall be written in base 10 representation")
@click.option("--iprange", 'iprange', default="none",
              help="--iprange=<iprange> Format accepted : A.B.C.D/E with A, B, C, D integers between 0 and 255, E between 0 and 32. They shall be written in base 10 representation")
def func_call(tool, ip, iprange):
    # Call the correct tool, pass the arguments
    """Simple program that can scan ip adress for vulnerability and exploit it"""
    if (ip == "none") and (iprange == "none"):
        click.echo("Please provide an IP adress (range) to use the tool on.")
        return -1
    if tool == "help":
        click.echo("This is a help message")
    elif tool == "nmap":
        click.echo("Calling nmap scripts...")
    elif tool == "heartbleed":
        click.echo("Calling heartbleed scripts...")
    elif tool == "shellshock":
        click.echo("Calling shellshock scripts...")
    else:
        click.echo("Unreachable code")


def main():
    ret = func_call()
    click.echo("Exited with error code : " + ret)
    return ret
