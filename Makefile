# Vanquish is a tool to recon and exploit heartbleed and shellshock vulnerability.
# Copyright (C) 2022  Théophile Reverdell, Isaïe Muron, Erwan Glaziou, Clara Delabrouille
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Makefile

setup: 
	# Install dependencies
	pip install -r requirements.txt
	# Install our package
	pip install .

run: setup
	# Run help command of cli tool
	python3 vanquish --help

test: setup
	# Run tests with print output (-s)
	python3 -m pytest -s

clean:
	# Remove cache folder
	rm -rf __pycache__
	# Remove test cache folder
	rm -rf tests/__pycache__

uninstall:
	pip uninstall -y vanquish

# Create virtualenv
venv_activate: requirements.txt 
	python3 -m venv .venv
	./.venv/bin/pip install -r requirements.txt
	./.venv/bin/pip install .

venv_run: venv_activate
	# Run help command of cli tool
	./.venv/bin/python3 vanquish --help

venv_deactivate:
	# Remove directory
	rm -rf ./.venv

